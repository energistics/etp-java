/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Etp.v12.Protocol.StoreNotification;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class ObjectAccessRevoked extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 5386982859036523774L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"ObjectAccessRevoked\",\"namespace\":\"Energistics.Etp.v12.Protocol.StoreNotification\",\"fields\":[{\"name\":\"uri\",\"type\":\"string\"},{\"name\":\"changeTime\",\"type\":\"long\"}],\"protocol\":\"5\",\"messageType\":\"5\",\"senderRole\":\"store\",\"protocolRoles\":\"store,customer\",\"multipartFlag\":false,\"fullName\":\"Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked\",\"depends\":[]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<ObjectAccessRevoked> ENCODER =
      new BinaryMessageEncoder<ObjectAccessRevoked>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<ObjectAccessRevoked> DECODER =
      new BinaryMessageDecoder<ObjectAccessRevoked>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<ObjectAccessRevoked> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<ObjectAccessRevoked> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<ObjectAccessRevoked>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this ObjectAccessRevoked to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a ObjectAccessRevoked from a ByteBuffer. */
  public static ObjectAccessRevoked fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.lang.CharSequence uri;
  @Deprecated public long changeTime;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public ObjectAccessRevoked() {}

  /**
   * All-args constructor.
   * @param uri The new value for uri
   * @param changeTime The new value for changeTime
   */
  public ObjectAccessRevoked(java.lang.CharSequence uri, java.lang.Long changeTime) {
    this.uri = uri;
    this.changeTime = changeTime;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return uri;
    case 1: return changeTime;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: uri = (java.lang.CharSequence)value$; break;
    case 1: changeTime = (java.lang.Long)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'uri' field.
   * @return The value of the 'uri' field.
   */
  public java.lang.CharSequence getUri() {
    return uri;
  }

  /**
   * Sets the value of the 'uri' field.
   * @param value the value to set.
   */
  public void setUri(java.lang.CharSequence value) {
    this.uri = value;
  }

  /**
   * Gets the value of the 'changeTime' field.
   * @return The value of the 'changeTime' field.
   */
  public java.lang.Long getChangeTime() {
    return changeTime;
  }

  /**
   * Sets the value of the 'changeTime' field.
   * @param value the value to set.
   */
  public void setChangeTime(java.lang.Long value) {
    this.changeTime = value;
  }

  /**
   * Creates a new ObjectAccessRevoked RecordBuilder.
   * @return A new ObjectAccessRevoked RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder newBuilder() {
    return new Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder();
  }

  /**
   * Creates a new ObjectAccessRevoked RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new ObjectAccessRevoked RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder newBuilder(Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder other) {
    return new Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder(other);
  }

  /**
   * Creates a new ObjectAccessRevoked RecordBuilder by copying an existing ObjectAccessRevoked instance.
   * @param other The existing instance to copy.
   * @return A new ObjectAccessRevoked RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder newBuilder(Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked other) {
    return new Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder(other);
  }

  /**
   * RecordBuilder for ObjectAccessRevoked instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<ObjectAccessRevoked>
    implements org.apache.avro.data.RecordBuilder<ObjectAccessRevoked> {

    private java.lang.CharSequence uri;
    private long changeTime;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.uri)) {
        this.uri = data().deepCopy(fields()[0].schema(), other.uri);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.changeTime)) {
        this.changeTime = data().deepCopy(fields()[1].schema(), other.changeTime);
        fieldSetFlags()[1] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing ObjectAccessRevoked instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.uri)) {
        this.uri = data().deepCopy(fields()[0].schema(), other.uri);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.changeTime)) {
        this.changeTime = data().deepCopy(fields()[1].schema(), other.changeTime);
        fieldSetFlags()[1] = true;
      }
    }

    /**
      * Gets the value of the 'uri' field.
      * @return The value.
      */
    public java.lang.CharSequence getUri() {
      return uri;
    }

    /**
      * Sets the value of the 'uri' field.
      * @param value The value of 'uri'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder setUri(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.uri = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'uri' field has been set.
      * @return True if the 'uri' field has been set, false otherwise.
      */
    public boolean hasUri() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'uri' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder clearUri() {
      uri = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'changeTime' field.
      * @return The value.
      */
    public java.lang.Long getChangeTime() {
      return changeTime;
    }

    /**
      * Sets the value of the 'changeTime' field.
      * @param value The value of 'changeTime'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder setChangeTime(long value) {
      validate(fields()[1], value);
      this.changeTime = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'changeTime' field has been set.
      * @return True if the 'changeTime' field has been set, false otherwise.
      */
    public boolean hasChangeTime() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'changeTime' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.StoreNotification.ObjectAccessRevoked.Builder clearChangeTime() {
      fieldSetFlags()[1] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ObjectAccessRevoked build() {
      try {
        ObjectAccessRevoked record = new ObjectAccessRevoked();
        record.uri = fieldSetFlags()[0] ? this.uri : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.changeTime = fieldSetFlags()[1] ? this.changeTime : (java.lang.Long) defaultValue(fields()[1]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<ObjectAccessRevoked>
    WRITER$ = (org.apache.avro.io.DatumWriter<ObjectAccessRevoked>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<ObjectAccessRevoked>
    READER$ = (org.apache.avro.io.DatumReader<ObjectAccessRevoked>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
