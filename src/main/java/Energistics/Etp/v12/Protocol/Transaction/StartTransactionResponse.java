/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Etp.v12.Protocol.Transaction;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class StartTransactionResponse extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -3146418116885775196L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"StartTransactionResponse\",\"namespace\":\"Energistics.Etp.v12.Protocol.Transaction\",\"fields\":[{\"name\":\"transactionUuid\",\"type\":{\"type\":\"fixed\",\"name\":\"Uuid\",\"namespace\":\"Energistics.Etp.v12.Datatypes\",\"size\":16,\"fullName\":\"Energistics.Etp.v12.Datatypes.Uuid\",\"depends\":[]}}],\"protocol\":\"18\",\"messageType\":\"2\",\"senderRole\":\"store\",\"protocolRoles\":\"store,customer\",\"multipartFlag\":false,\"fullName\":\"Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse\",\"depends\":[\"Energistics.Etp.v12.Datatypes.Uuid\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<StartTransactionResponse> ENCODER =
      new BinaryMessageEncoder<StartTransactionResponse>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<StartTransactionResponse> DECODER =
      new BinaryMessageDecoder<StartTransactionResponse>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<StartTransactionResponse> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<StartTransactionResponse> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<StartTransactionResponse>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this StartTransactionResponse to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a StartTransactionResponse from a ByteBuffer. */
  public static StartTransactionResponse fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public Energistics.Etp.v12.Datatypes.Uuid transactionUuid;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public StartTransactionResponse() {}

  /**
   * All-args constructor.
   * @param transactionUuid The new value for transactionUuid
   */
  public StartTransactionResponse(Energistics.Etp.v12.Datatypes.Uuid transactionUuid) {
    this.transactionUuid = transactionUuid;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return transactionUuid;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: transactionUuid = (Energistics.Etp.v12.Datatypes.Uuid)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'transactionUuid' field.
   * @return The value of the 'transactionUuid' field.
   */
  public Energistics.Etp.v12.Datatypes.Uuid getTransactionUuid() {
    return transactionUuid;
  }

  /**
   * Sets the value of the 'transactionUuid' field.
   * @param value the value to set.
   */
  public void setTransactionUuid(Energistics.Etp.v12.Datatypes.Uuid value) {
    this.transactionUuid = value;
  }

  /**
   * Creates a new StartTransactionResponse RecordBuilder.
   * @return A new StartTransactionResponse RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder newBuilder() {
    return new Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder();
  }

  /**
   * Creates a new StartTransactionResponse RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new StartTransactionResponse RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder newBuilder(Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder other) {
    return new Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder(other);
  }

  /**
   * Creates a new StartTransactionResponse RecordBuilder by copying an existing StartTransactionResponse instance.
   * @param other The existing instance to copy.
   * @return A new StartTransactionResponse RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder newBuilder(Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse other) {
    return new Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder(other);
  }

  /**
   * RecordBuilder for StartTransactionResponse instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<StartTransactionResponse>
    implements org.apache.avro.data.RecordBuilder<StartTransactionResponse> {

    private Energistics.Etp.v12.Datatypes.Uuid transactionUuid;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.transactionUuid)) {
        this.transactionUuid = data().deepCopy(fields()[0].schema(), other.transactionUuid);
        fieldSetFlags()[0] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing StartTransactionResponse instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.transactionUuid)) {
        this.transactionUuid = data().deepCopy(fields()[0].schema(), other.transactionUuid);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'transactionUuid' field.
      * @return The value.
      */
    public Energistics.Etp.v12.Datatypes.Uuid getTransactionUuid() {
      return transactionUuid;
    }

    /**
      * Sets the value of the 'transactionUuid' field.
      * @param value The value of 'transactionUuid'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder setTransactionUuid(Energistics.Etp.v12.Datatypes.Uuid value) {
      validate(fields()[0], value);
      this.transactionUuid = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'transactionUuid' field has been set.
      * @return True if the 'transactionUuid' field has been set, false otherwise.
      */
    public boolean hasTransactionUuid() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'transactionUuid' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Transaction.StartTransactionResponse.Builder clearTransactionUuid() {
      transactionUuid = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public StartTransactionResponse build() {
      try {
        StartTransactionResponse record = new StartTransactionResponse();
        record.transactionUuid = fieldSetFlags()[0] ? this.transactionUuid : (Energistics.Etp.v12.Datatypes.Uuid) defaultValue(fields()[0]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<StartTransactionResponse>
    WRITER$ = (org.apache.avro.io.DatumWriter<StartTransactionResponse>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<StartTransactionResponse>
    READER$ = (org.apache.avro.io.DatumReader<StartTransactionResponse>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
