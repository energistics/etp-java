/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Etp.v12.Protocol.Core;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class RequestSession extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 3499774184948235492L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"RequestSession\",\"namespace\":\"Energistics.Etp.v12.Protocol.Core\",\"fields\":[{\"name\":\"applicationName\",\"type\":\"string\"},{\"name\":\"applicationVersion\",\"type\":\"string\"},{\"name\":\"requestedProtocols\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"SupportedProtocol\",\"namespace\":\"Energistics.Etp.v12.Datatypes\",\"fields\":[{\"name\":\"protocol\",\"type\":\"int\"},{\"name\":\"protocolVersion\",\"type\":{\"type\":\"record\",\"name\":\"Version\",\"fields\":[{\"name\":\"major\",\"type\":\"int\",\"default\":0},{\"name\":\"minor\",\"type\":\"int\",\"default\":0},{\"name\":\"revision\",\"type\":\"int\",\"default\":0},{\"name\":\"patch\",\"type\":\"int\",\"default\":0}],\"fullName\":\"Energistics.Etp.v12.Datatypes.Version\",\"depends\":[]}},{\"name\":\"role\",\"type\":\"string\"},{\"name\":\"protocolCapabilities\",\"type\":{\"type\":\"map\",\"values\":{\"type\":\"record\",\"name\":\"DataValue\",\"fields\":[{\"name\":\"item\",\"type\":[\"null\",\"boolean\",\"int\",\"long\",\"float\",\"double\",\"string\",{\"type\":\"record\",\"name\":\"ArrayOfBoolean\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"boolean\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfBoolean\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfInt\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"int\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfInt\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfLong\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"long\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfLong\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfFloat\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"float\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfFloat\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfDouble\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"double\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfDouble\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfString\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"string\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfString\",\"depends\":[]},\"bytes\"]}],\"fullName\":\"Energistics.Etp.v12.Datatypes.DataValue\",\"depends\":[\"Energistics.Etp.v12.Datatypes.ArrayOfBoolean\",\"Energistics.Etp.v12.Datatypes.ArrayOfInt\",\"Energistics.Etp.v12.Datatypes.ArrayOfLong\",\"Energistics.Etp.v12.Datatypes.ArrayOfFloat\",\"Energistics.Etp.v12.Datatypes.ArrayOfDouble\",\"Energistics.Etp.v12.Datatypes.ArrayOfString\"]}}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.SupportedProtocol\",\"depends\":[\"Energistics.Etp.v12.Datatypes.Version\",\"Energistics.Etp.v12.Datatypes.DataValue\"]}}},{\"name\":\"supportedObjects\",\"type\":{\"type\":\"array\",\"items\":\"string\"}},{\"name\":\"supportedCompression\",\"type\":\"string\"}],\"protocol\":\"0\",\"messageType\":\"1\",\"senderRole\":\"client\",\"protocolRoles\":\"client, server\",\"multipartFlag\":false,\"fullName\":\"Energistics.Etp.v12.Protocol.Core.RequestSession\",\"depends\":[\"Energistics.Etp.v12.Datatypes.SupportedProtocol\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<RequestSession> ENCODER =
      new BinaryMessageEncoder<RequestSession>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<RequestSession> DECODER =
      new BinaryMessageDecoder<RequestSession>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<RequestSession> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<RequestSession> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<RequestSession>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this RequestSession to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a RequestSession from a ByteBuffer. */
  public static RequestSession fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.lang.CharSequence applicationName;
  @Deprecated public java.lang.CharSequence applicationVersion;
  @Deprecated public java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol> requestedProtocols;
  @Deprecated public java.util.List<java.lang.CharSequence> supportedObjects;
  @Deprecated public java.lang.CharSequence supportedCompression;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public RequestSession() {}

  /**
   * All-args constructor.
   * @param applicationName The new value for applicationName
   * @param applicationVersion The new value for applicationVersion
   * @param requestedProtocols The new value for requestedProtocols
   * @param supportedObjects The new value for supportedObjects
   * @param supportedCompression The new value for supportedCompression
   */
  public RequestSession(java.lang.CharSequence applicationName, java.lang.CharSequence applicationVersion, java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol> requestedProtocols, java.util.List<java.lang.CharSequence> supportedObjects, java.lang.CharSequence supportedCompression) {
    this.applicationName = applicationName;
    this.applicationVersion = applicationVersion;
    this.requestedProtocols = requestedProtocols;
    this.supportedObjects = supportedObjects;
    this.supportedCompression = supportedCompression;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return applicationName;
    case 1: return applicationVersion;
    case 2: return requestedProtocols;
    case 3: return supportedObjects;
    case 4: return supportedCompression;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: applicationName = (java.lang.CharSequence)value$; break;
    case 1: applicationVersion = (java.lang.CharSequence)value$; break;
    case 2: requestedProtocols = (java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol>)value$; break;
    case 3: supportedObjects = (java.util.List<java.lang.CharSequence>)value$; break;
    case 4: supportedCompression = (java.lang.CharSequence)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'applicationName' field.
   * @return The value of the 'applicationName' field.
   */
  public java.lang.CharSequence getApplicationName() {
    return applicationName;
  }

  /**
   * Sets the value of the 'applicationName' field.
   * @param value the value to set.
   */
  public void setApplicationName(java.lang.CharSequence value) {
    this.applicationName = value;
  }

  /**
   * Gets the value of the 'applicationVersion' field.
   * @return The value of the 'applicationVersion' field.
   */
  public java.lang.CharSequence getApplicationVersion() {
    return applicationVersion;
  }

  /**
   * Sets the value of the 'applicationVersion' field.
   * @param value the value to set.
   */
  public void setApplicationVersion(java.lang.CharSequence value) {
    this.applicationVersion = value;
  }

  /**
   * Gets the value of the 'requestedProtocols' field.
   * @return The value of the 'requestedProtocols' field.
   */
  public java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol> getRequestedProtocols() {
    return requestedProtocols;
  }

  /**
   * Sets the value of the 'requestedProtocols' field.
   * @param value the value to set.
   */
  public void setRequestedProtocols(java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol> value) {
    this.requestedProtocols = value;
  }

  /**
   * Gets the value of the 'supportedObjects' field.
   * @return The value of the 'supportedObjects' field.
   */
  public java.util.List<java.lang.CharSequence> getSupportedObjects() {
    return supportedObjects;
  }

  /**
   * Sets the value of the 'supportedObjects' field.
   * @param value the value to set.
   */
  public void setSupportedObjects(java.util.List<java.lang.CharSequence> value) {
    this.supportedObjects = value;
  }

  /**
   * Gets the value of the 'supportedCompression' field.
   * @return The value of the 'supportedCompression' field.
   */
  public java.lang.CharSequence getSupportedCompression() {
    return supportedCompression;
  }

  /**
   * Sets the value of the 'supportedCompression' field.
   * @param value the value to set.
   */
  public void setSupportedCompression(java.lang.CharSequence value) {
    this.supportedCompression = value;
  }

  /**
   * Creates a new RequestSession RecordBuilder.
   * @return A new RequestSession RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.Core.RequestSession.Builder newBuilder() {
    return new Energistics.Etp.v12.Protocol.Core.RequestSession.Builder();
  }

  /**
   * Creates a new RequestSession RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new RequestSession RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.Core.RequestSession.Builder newBuilder(Energistics.Etp.v12.Protocol.Core.RequestSession.Builder other) {
    return new Energistics.Etp.v12.Protocol.Core.RequestSession.Builder(other);
  }

  /**
   * Creates a new RequestSession RecordBuilder by copying an existing RequestSession instance.
   * @param other The existing instance to copy.
   * @return A new RequestSession RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.Core.RequestSession.Builder newBuilder(Energistics.Etp.v12.Protocol.Core.RequestSession other) {
    return new Energistics.Etp.v12.Protocol.Core.RequestSession.Builder(other);
  }

  /**
   * RecordBuilder for RequestSession instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<RequestSession>
    implements org.apache.avro.data.RecordBuilder<RequestSession> {

    private java.lang.CharSequence applicationName;
    private java.lang.CharSequence applicationVersion;
    private java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol> requestedProtocols;
    private java.util.List<java.lang.CharSequence> supportedObjects;
    private java.lang.CharSequence supportedCompression;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Etp.v12.Protocol.Core.RequestSession.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.applicationName)) {
        this.applicationName = data().deepCopy(fields()[0].schema(), other.applicationName);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.applicationVersion)) {
        this.applicationVersion = data().deepCopy(fields()[1].schema(), other.applicationVersion);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.requestedProtocols)) {
        this.requestedProtocols = data().deepCopy(fields()[2].schema(), other.requestedProtocols);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.supportedObjects)) {
        this.supportedObjects = data().deepCopy(fields()[3].schema(), other.supportedObjects);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.supportedCompression)) {
        this.supportedCompression = data().deepCopy(fields()[4].schema(), other.supportedCompression);
        fieldSetFlags()[4] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing RequestSession instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Etp.v12.Protocol.Core.RequestSession other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.applicationName)) {
        this.applicationName = data().deepCopy(fields()[0].schema(), other.applicationName);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.applicationVersion)) {
        this.applicationVersion = data().deepCopy(fields()[1].schema(), other.applicationVersion);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.requestedProtocols)) {
        this.requestedProtocols = data().deepCopy(fields()[2].schema(), other.requestedProtocols);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.supportedObjects)) {
        this.supportedObjects = data().deepCopy(fields()[3].schema(), other.supportedObjects);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.supportedCompression)) {
        this.supportedCompression = data().deepCopy(fields()[4].schema(), other.supportedCompression);
        fieldSetFlags()[4] = true;
      }
    }

    /**
      * Gets the value of the 'applicationName' field.
      * @return The value.
      */
    public java.lang.CharSequence getApplicationName() {
      return applicationName;
    }

    /**
      * Sets the value of the 'applicationName' field.
      * @param value The value of 'applicationName'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder setApplicationName(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.applicationName = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'applicationName' field has been set.
      * @return True if the 'applicationName' field has been set, false otherwise.
      */
    public boolean hasApplicationName() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'applicationName' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder clearApplicationName() {
      applicationName = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'applicationVersion' field.
      * @return The value.
      */
    public java.lang.CharSequence getApplicationVersion() {
      return applicationVersion;
    }

    /**
      * Sets the value of the 'applicationVersion' field.
      * @param value The value of 'applicationVersion'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder setApplicationVersion(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.applicationVersion = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'applicationVersion' field has been set.
      * @return True if the 'applicationVersion' field has been set, false otherwise.
      */
    public boolean hasApplicationVersion() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'applicationVersion' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder clearApplicationVersion() {
      applicationVersion = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'requestedProtocols' field.
      * @return The value.
      */
    public java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol> getRequestedProtocols() {
      return requestedProtocols;
    }

    /**
      * Sets the value of the 'requestedProtocols' field.
      * @param value The value of 'requestedProtocols'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder setRequestedProtocols(java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol> value) {
      validate(fields()[2], value);
      this.requestedProtocols = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'requestedProtocols' field has been set.
      * @return True if the 'requestedProtocols' field has been set, false otherwise.
      */
    public boolean hasRequestedProtocols() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'requestedProtocols' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder clearRequestedProtocols() {
      requestedProtocols = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'supportedObjects' field.
      * @return The value.
      */
    public java.util.List<java.lang.CharSequence> getSupportedObjects() {
      return supportedObjects;
    }

    /**
      * Sets the value of the 'supportedObjects' field.
      * @param value The value of 'supportedObjects'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder setSupportedObjects(java.util.List<java.lang.CharSequence> value) {
      validate(fields()[3], value);
      this.supportedObjects = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'supportedObjects' field has been set.
      * @return True if the 'supportedObjects' field has been set, false otherwise.
      */
    public boolean hasSupportedObjects() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'supportedObjects' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder clearSupportedObjects() {
      supportedObjects = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'supportedCompression' field.
      * @return The value.
      */
    public java.lang.CharSequence getSupportedCompression() {
      return supportedCompression;
    }

    /**
      * Sets the value of the 'supportedCompression' field.
      * @param value The value of 'supportedCompression'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder setSupportedCompression(java.lang.CharSequence value) {
      validate(fields()[4], value);
      this.supportedCompression = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'supportedCompression' field has been set.
      * @return True if the 'supportedCompression' field has been set, false otherwise.
      */
    public boolean hasSupportedCompression() {
      return fieldSetFlags()[4];
    }


    /**
      * Clears the value of the 'supportedCompression' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.Core.RequestSession.Builder clearSupportedCompression() {
      supportedCompression = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public RequestSession build() {
      try {
        RequestSession record = new RequestSession();
        record.applicationName = fieldSetFlags()[0] ? this.applicationName : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.applicationVersion = fieldSetFlags()[1] ? this.applicationVersion : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.requestedProtocols = fieldSetFlags()[2] ? this.requestedProtocols : (java.util.List<Energistics.Etp.v12.Datatypes.SupportedProtocol>) defaultValue(fields()[2]);
        record.supportedObjects = fieldSetFlags()[3] ? this.supportedObjects : (java.util.List<java.lang.CharSequence>) defaultValue(fields()[3]);
        record.supportedCompression = fieldSetFlags()[4] ? this.supportedCompression : (java.lang.CharSequence) defaultValue(fields()[4]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<RequestSession>
    WRITER$ = (org.apache.avro.io.DatumWriter<RequestSession>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<RequestSession>
    READER$ = (org.apache.avro.io.DatumReader<RequestSession>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
