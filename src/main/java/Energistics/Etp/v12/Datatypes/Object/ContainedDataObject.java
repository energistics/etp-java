/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Etp.v12.Datatypes.Object;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class ContainedDataObject extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -4935121738994893436L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"ContainedDataObject\",\"namespace\":\"Energistics.Etp.v12.Datatypes.Object\",\"fields\":[{\"name\":\"resource\",\"type\":{\"type\":\"record\",\"name\":\"Resource\",\"fields\":[{\"name\":\"uri\",\"type\":\"string\"},{\"name\":\"contentType\",\"type\":\"string\"},{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"objectNotifiable\",\"type\":\"boolean\",\"default\":true},{\"name\":\"resourceType\",\"type\":{\"type\":\"enum\",\"name\":\"ResourceKind\",\"symbols\":[\"DataObject\",\"Folder\",\"UriProtocol\",\"DataSpace\"],\"fullName\":\"Energistics.Etp.v12.Datatypes.Object.ResourceKind\",\"depends\":[]}},{\"name\":\"sourceCount\",\"type\":[\"null\",\"int\"],\"default\":null},{\"name\":\"targetCount\",\"type\":[\"null\",\"int\"],\"default\":null},{\"name\":\"contentCount\",\"type\":[\"null\",\"int\"],\"default\":null},{\"name\":\"lastChanged\",\"type\":[\"null\",\"long\"]},{\"name\":\"customData\",\"type\":{\"type\":\"map\",\"values\":\"string\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.Object.Resource\",\"depends\":[\"Energistics.Etp.v12.Datatypes.Object.ResourceKind\"]}},{\"name\":\"data\",\"type\":\"bytes\"},{\"name\":\"ContainerIDs\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"fixed\",\"name\":\"Uuid\",\"namespace\":\"Energistics.Etp.v12.Datatypes\",\"size\":16,\"fullName\":\"Energistics.Etp.v12.Datatypes.Uuid\",\"depends\":[]}}},{\"name\":\"ContainsIDs\",\"type\":{\"type\":\"array\",\"items\":\"Energistics.Etp.v12.Datatypes.Uuid\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.Object.ContainedDataObject\",\"depends\":[\"Energistics.Etp.v12.Datatypes.Object.Resource\",\"Energistics.Etp.v12.Datatypes.Uuid\",\"Energistics.Etp.v12.Datatypes.Uuid\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<ContainedDataObject> ENCODER =
      new BinaryMessageEncoder<ContainedDataObject>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<ContainedDataObject> DECODER =
      new BinaryMessageDecoder<ContainedDataObject>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<ContainedDataObject> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<ContainedDataObject> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<ContainedDataObject>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this ContainedDataObject to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a ContainedDataObject from a ByteBuffer. */
  public static ContainedDataObject fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public Energistics.Etp.v12.Datatypes.Object.Resource resource;
  @Deprecated public java.nio.ByteBuffer data;
  @Deprecated public java.util.List<Energistics.Etp.v12.Datatypes.Uuid> ContainerIDs;
  @Deprecated public java.util.List<Energistics.Etp.v12.Datatypes.Uuid> ContainsIDs;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public ContainedDataObject() {}

  /**
   * All-args constructor.
   * @param resource The new value for resource
   * @param data The new value for data
   * @param ContainerIDs The new value for ContainerIDs
   * @param ContainsIDs The new value for ContainsIDs
   */
  public ContainedDataObject(Energistics.Etp.v12.Datatypes.Object.Resource resource, java.nio.ByteBuffer data, java.util.List<Energistics.Etp.v12.Datatypes.Uuid> ContainerIDs, java.util.List<Energistics.Etp.v12.Datatypes.Uuid> ContainsIDs) {
    this.resource = resource;
    this.data = data;
    this.ContainerIDs = ContainerIDs;
    this.ContainsIDs = ContainsIDs;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return resource;
    case 1: return data;
    case 2: return ContainerIDs;
    case 3: return ContainsIDs;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: resource = (Energistics.Etp.v12.Datatypes.Object.Resource)value$; break;
    case 1: data = (java.nio.ByteBuffer)value$; break;
    case 2: ContainerIDs = (java.util.List<Energistics.Etp.v12.Datatypes.Uuid>)value$; break;
    case 3: ContainsIDs = (java.util.List<Energistics.Etp.v12.Datatypes.Uuid>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'resource' field.
   * @return The value of the 'resource' field.
   */
  public Energistics.Etp.v12.Datatypes.Object.Resource getResource() {
    return resource;
  }

  /**
   * Sets the value of the 'resource' field.
   * @param value the value to set.
   */
  public void setResource(Energistics.Etp.v12.Datatypes.Object.Resource value) {
    this.resource = value;
  }

  /**
   * Gets the value of the 'data' field.
   * @return The value of the 'data' field.
   */
  public java.nio.ByteBuffer getData() {
    return data;
  }

  /**
   * Sets the value of the 'data' field.
   * @param value the value to set.
   */
  public void setData(java.nio.ByteBuffer value) {
    this.data = value;
  }

  /**
   * Gets the value of the 'ContainerIDs' field.
   * @return The value of the 'ContainerIDs' field.
   */
  public java.util.List<Energistics.Etp.v12.Datatypes.Uuid> getContainerIDs() {
    return ContainerIDs;
  }

  /**
   * Sets the value of the 'ContainerIDs' field.
   * @param value the value to set.
   */
  public void setContainerIDs(java.util.List<Energistics.Etp.v12.Datatypes.Uuid> value) {
    this.ContainerIDs = value;
  }

  /**
   * Gets the value of the 'ContainsIDs' field.
   * @return The value of the 'ContainsIDs' field.
   */
  public java.util.List<Energistics.Etp.v12.Datatypes.Uuid> getContainsIDs() {
    return ContainsIDs;
  }

  /**
   * Sets the value of the 'ContainsIDs' field.
   * @param value the value to set.
   */
  public void setContainsIDs(java.util.List<Energistics.Etp.v12.Datatypes.Uuid> value) {
    this.ContainsIDs = value;
  }

  /**
   * Creates a new ContainedDataObject RecordBuilder.
   * @return A new ContainedDataObject RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder newBuilder() {
    return new Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder();
  }

  /**
   * Creates a new ContainedDataObject RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new ContainedDataObject RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder newBuilder(Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder other) {
    return new Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder(other);
  }

  /**
   * Creates a new ContainedDataObject RecordBuilder by copying an existing ContainedDataObject instance.
   * @param other The existing instance to copy.
   * @return A new ContainedDataObject RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder newBuilder(Energistics.Etp.v12.Datatypes.Object.ContainedDataObject other) {
    return new Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder(other);
  }

  /**
   * RecordBuilder for ContainedDataObject instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<ContainedDataObject>
    implements org.apache.avro.data.RecordBuilder<ContainedDataObject> {

    private Energistics.Etp.v12.Datatypes.Object.Resource resource;
    private Energistics.Etp.v12.Datatypes.Object.Resource.Builder resourceBuilder;
    private java.nio.ByteBuffer data;
    private java.util.List<Energistics.Etp.v12.Datatypes.Uuid> ContainerIDs;
    private java.util.List<Energistics.Etp.v12.Datatypes.Uuid> ContainsIDs;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.resource)) {
        this.resource = data().deepCopy(fields()[0].schema(), other.resource);
        fieldSetFlags()[0] = true;
      }
      if (other.hasResourceBuilder()) {
        this.resourceBuilder = Energistics.Etp.v12.Datatypes.Object.Resource.newBuilder(other.getResourceBuilder());
      }
      if (isValidValue(fields()[1], other.data)) {
        this.data = data().deepCopy(fields()[1].schema(), other.data);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.ContainerIDs)) {
        this.ContainerIDs = data().deepCopy(fields()[2].schema(), other.ContainerIDs);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.ContainsIDs)) {
        this.ContainsIDs = data().deepCopy(fields()[3].schema(), other.ContainsIDs);
        fieldSetFlags()[3] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing ContainedDataObject instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Etp.v12.Datatypes.Object.ContainedDataObject other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.resource)) {
        this.resource = data().deepCopy(fields()[0].schema(), other.resource);
        fieldSetFlags()[0] = true;
      }
      this.resourceBuilder = null;
      if (isValidValue(fields()[1], other.data)) {
        this.data = data().deepCopy(fields()[1].schema(), other.data);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.ContainerIDs)) {
        this.ContainerIDs = data().deepCopy(fields()[2].schema(), other.ContainerIDs);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.ContainsIDs)) {
        this.ContainsIDs = data().deepCopy(fields()[3].schema(), other.ContainsIDs);
        fieldSetFlags()[3] = true;
      }
    }

    /**
      * Gets the value of the 'resource' field.
      * @return The value.
      */
    public Energistics.Etp.v12.Datatypes.Object.Resource getResource() {
      return resource;
    }

    /**
      * Sets the value of the 'resource' field.
      * @param value The value of 'resource'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder setResource(Energistics.Etp.v12.Datatypes.Object.Resource value) {
      validate(fields()[0], value);
      this.resourceBuilder = null;
      this.resource = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'resource' field has been set.
      * @return True if the 'resource' field has been set, false otherwise.
      */
    public boolean hasResource() {
      return fieldSetFlags()[0];
    }

    /**
     * Gets the Builder instance for the 'resource' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public Energistics.Etp.v12.Datatypes.Object.Resource.Builder getResourceBuilder() {
      if (resourceBuilder == null) {
        if (hasResource()) {
          setResourceBuilder(Energistics.Etp.v12.Datatypes.Object.Resource.newBuilder(resource));
        } else {
          setResourceBuilder(Energistics.Etp.v12.Datatypes.Object.Resource.newBuilder());
        }
      }
      return resourceBuilder;
    }

    /**
     * Sets the Builder instance for the 'resource' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder setResourceBuilder(Energistics.Etp.v12.Datatypes.Object.Resource.Builder value) {
      clearResource();
      resourceBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'resource' field has an active Builder instance
     * @return True if the 'resource' field has an active Builder instance
     */
    public boolean hasResourceBuilder() {
      return resourceBuilder != null;
    }

    /**
      * Clears the value of the 'resource' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder clearResource() {
      resource = null;
      resourceBuilder = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'data' field.
      * @return The value.
      */
    public java.nio.ByteBuffer getData() {
      return data;
    }

    /**
      * Sets the value of the 'data' field.
      * @param value The value of 'data'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder setData(java.nio.ByteBuffer value) {
      validate(fields()[1], value);
      this.data = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'data' field has been set.
      * @return True if the 'data' field has been set, false otherwise.
      */
    public boolean hasData() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'data' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder clearData() {
      data = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'ContainerIDs' field.
      * @return The value.
      */
    public java.util.List<Energistics.Etp.v12.Datatypes.Uuid> getContainerIDs() {
      return ContainerIDs;
    }

    /**
      * Sets the value of the 'ContainerIDs' field.
      * @param value The value of 'ContainerIDs'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder setContainerIDs(java.util.List<Energistics.Etp.v12.Datatypes.Uuid> value) {
      validate(fields()[2], value);
      this.ContainerIDs = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'ContainerIDs' field has been set.
      * @return True if the 'ContainerIDs' field has been set, false otherwise.
      */
    public boolean hasContainerIDs() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'ContainerIDs' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder clearContainerIDs() {
      ContainerIDs = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'ContainsIDs' field.
      * @return The value.
      */
    public java.util.List<Energistics.Etp.v12.Datatypes.Uuid> getContainsIDs() {
      return ContainsIDs;
    }

    /**
      * Sets the value of the 'ContainsIDs' field.
      * @param value The value of 'ContainsIDs'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder setContainsIDs(java.util.List<Energistics.Etp.v12.Datatypes.Uuid> value) {
      validate(fields()[3], value);
      this.ContainsIDs = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'ContainsIDs' field has been set.
      * @return True if the 'ContainsIDs' field has been set, false otherwise.
      */
    public boolean hasContainsIDs() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'ContainsIDs' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.ContainedDataObject.Builder clearContainsIDs() {
      ContainsIDs = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ContainedDataObject build() {
      try {
        ContainedDataObject record = new ContainedDataObject();
        if (resourceBuilder != null) {
          record.resource = this.resourceBuilder.build();
        } else {
          record.resource = fieldSetFlags()[0] ? this.resource : (Energistics.Etp.v12.Datatypes.Object.Resource) defaultValue(fields()[0]);
        }
        record.data = fieldSetFlags()[1] ? this.data : (java.nio.ByteBuffer) defaultValue(fields()[1]);
        record.ContainerIDs = fieldSetFlags()[2] ? this.ContainerIDs : (java.util.List<Energistics.Etp.v12.Datatypes.Uuid>) defaultValue(fields()[2]);
        record.ContainsIDs = fieldSetFlags()[3] ? this.ContainsIDs : (java.util.List<Energistics.Etp.v12.Datatypes.Uuid>) defaultValue(fields()[3]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<ContainedDataObject>
    WRITER$ = (org.apache.avro.io.DatumWriter<ContainedDataObject>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<ContainedDataObject>
    READER$ = (org.apache.avro.io.DatumReader<ContainedDataObject>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
