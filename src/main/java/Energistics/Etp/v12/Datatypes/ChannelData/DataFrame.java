/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Etp.v12.Datatypes.ChannelData;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class DataFrame extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -3254441851153880332L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"DataFrame\",\"namespace\":\"Energistics.Etp.v12.Datatypes.ChannelData\",\"fields\":[{\"name\":\"indexes\",\"type\":{\"type\":\"array\",\"items\":\"long\"}},{\"name\":\"data\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"DataValue\",\"namespace\":\"Energistics.Etp.v12.Datatypes\",\"fields\":[{\"name\":\"item\",\"type\":[\"null\",\"boolean\",\"int\",\"long\",\"float\",\"double\",\"string\",{\"type\":\"record\",\"name\":\"ArrayOfBoolean\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"boolean\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfBoolean\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfInt\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"int\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfInt\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfLong\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"long\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfLong\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfFloat\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"float\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfFloat\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfDouble\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"double\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfDouble\",\"depends\":[]},{\"type\":\"record\",\"name\":\"ArrayOfString\",\"fields\":[{\"name\":\"values\",\"type\":{\"type\":\"array\",\"items\":\"string\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ArrayOfString\",\"depends\":[]},\"bytes\"]}],\"fullName\":\"Energistics.Etp.v12.Datatypes.DataValue\",\"depends\":[\"Energistics.Etp.v12.Datatypes.ArrayOfBoolean\",\"Energistics.Etp.v12.Datatypes.ArrayOfInt\",\"Energistics.Etp.v12.Datatypes.ArrayOfLong\",\"Energistics.Etp.v12.Datatypes.ArrayOfFloat\",\"Energistics.Etp.v12.Datatypes.ArrayOfDouble\",\"Energistics.Etp.v12.Datatypes.ArrayOfString\"]}}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.ChannelData.DataFrame\",\"depends\":[\"Energistics.Etp.v12.Datatypes.DataValue\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<DataFrame> ENCODER =
      new BinaryMessageEncoder<DataFrame>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<DataFrame> DECODER =
      new BinaryMessageDecoder<DataFrame>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<DataFrame> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<DataFrame> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<DataFrame>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this DataFrame to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a DataFrame from a ByteBuffer. */
  public static DataFrame fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.util.List<java.lang.Long> indexes;
  @Deprecated public java.util.List<Energistics.Etp.v12.Datatypes.DataValue> data;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public DataFrame() {}

  /**
   * All-args constructor.
   * @param indexes The new value for indexes
   * @param data The new value for data
   */
  public DataFrame(java.util.List<java.lang.Long> indexes, java.util.List<Energistics.Etp.v12.Datatypes.DataValue> data) {
    this.indexes = indexes;
    this.data = data;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return indexes;
    case 1: return data;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: indexes = (java.util.List<java.lang.Long>)value$; break;
    case 1: data = (java.util.List<Energistics.Etp.v12.Datatypes.DataValue>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'indexes' field.
   * @return The value of the 'indexes' field.
   */
  public java.util.List<java.lang.Long> getIndexes() {
    return indexes;
  }

  /**
   * Sets the value of the 'indexes' field.
   * @param value the value to set.
   */
  public void setIndexes(java.util.List<java.lang.Long> value) {
    this.indexes = value;
  }

  /**
   * Gets the value of the 'data' field.
   * @return The value of the 'data' field.
   */
  public java.util.List<Energistics.Etp.v12.Datatypes.DataValue> getData() {
    return data;
  }

  /**
   * Sets the value of the 'data' field.
   * @param value the value to set.
   */
  public void setData(java.util.List<Energistics.Etp.v12.Datatypes.DataValue> value) {
    this.data = value;
  }

  /**
   * Creates a new DataFrame RecordBuilder.
   * @return A new DataFrame RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder newBuilder() {
    return new Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder();
  }

  /**
   * Creates a new DataFrame RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new DataFrame RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder newBuilder(Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder other) {
    return new Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder(other);
  }

  /**
   * Creates a new DataFrame RecordBuilder by copying an existing DataFrame instance.
   * @param other The existing instance to copy.
   * @return A new DataFrame RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder newBuilder(Energistics.Etp.v12.Datatypes.ChannelData.DataFrame other) {
    return new Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder(other);
  }

  /**
   * RecordBuilder for DataFrame instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<DataFrame>
    implements org.apache.avro.data.RecordBuilder<DataFrame> {

    private java.util.List<java.lang.Long> indexes;
    private java.util.List<Energistics.Etp.v12.Datatypes.DataValue> data;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.indexes)) {
        this.indexes = data().deepCopy(fields()[0].schema(), other.indexes);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.data)) {
        this.data = data().deepCopy(fields()[1].schema(), other.data);
        fieldSetFlags()[1] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing DataFrame instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Etp.v12.Datatypes.ChannelData.DataFrame other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.indexes)) {
        this.indexes = data().deepCopy(fields()[0].schema(), other.indexes);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.data)) {
        this.data = data().deepCopy(fields()[1].schema(), other.data);
        fieldSetFlags()[1] = true;
      }
    }

    /**
      * Gets the value of the 'indexes' field.
      * @return The value.
      */
    public java.util.List<java.lang.Long> getIndexes() {
      return indexes;
    }

    /**
      * Sets the value of the 'indexes' field.
      * @param value The value of 'indexes'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder setIndexes(java.util.List<java.lang.Long> value) {
      validate(fields()[0], value);
      this.indexes = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'indexes' field has been set.
      * @return True if the 'indexes' field has been set, false otherwise.
      */
    public boolean hasIndexes() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'indexes' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder clearIndexes() {
      indexes = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'data' field.
      * @return The value.
      */
    public java.util.List<Energistics.Etp.v12.Datatypes.DataValue> getData() {
      return data;
    }

    /**
      * Sets the value of the 'data' field.
      * @param value The value of 'data'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder setData(java.util.List<Energistics.Etp.v12.Datatypes.DataValue> value) {
      validate(fields()[1], value);
      this.data = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'data' field has been set.
      * @return True if the 'data' field has been set, false otherwise.
      */
    public boolean hasData() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'data' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.ChannelData.DataFrame.Builder clearData() {
      data = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public DataFrame build() {
      try {
        DataFrame record = new DataFrame();
        record.indexes = fieldSetFlags()[0] ? this.indexes : (java.util.List<java.lang.Long>) defaultValue(fields()[0]);
        record.data = fieldSetFlags()[1] ? this.data : (java.util.List<Energistics.Etp.v12.Datatypes.DataValue>) defaultValue(fields()[1]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<DataFrame>
    WRITER$ = (org.apache.avro.io.DatumWriter<DataFrame>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<DataFrame>
    READER$ = (org.apache.avro.io.DatumReader<DataFrame>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
